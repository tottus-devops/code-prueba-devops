FROM node:14-alpine
WORKDIR /app
EXPOSE 3000
COPY ./package.json ./
RUN npm install --only=production
COPY index.js index.js
RUN addgroup -S devops && adduser -S devops -G devops
RUN chown -R devops:devops /app
USER devops
CMD [ "npm", "start" ]
