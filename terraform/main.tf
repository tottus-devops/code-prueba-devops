terraform {
  backend "azurerm" {}
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
}

resource "kubernetes_namespace" "k8s_namespace" {
  metadata {
    name = "node-app-ns"
  }
}

resource "kubernetes_role" "k8s_role" {
  metadata {
    name = "terraform-k8s-role"
    namespace = kubernetes_namespace.k8s_namespace.metadata.0.name
  }

  rule {
    api_groups     = [""]
    resources      = ["*"]
    verbs          = ["*"]
  }

}

resource "kubernetes_service_account" "k8s_service_account" {
  metadata {
    name = "terraform-k8s-service-account"
    namespace = kubernetes_namespace.k8s_namespace.metadata.0.name
  }
}

resource "kubernetes_role_binding" "k8s_role_binding" {
  metadata {
    name      = "terraform-k8s-role-binding"
    namespace = kubernetes_namespace.k8s_namespace.metadata.0.name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.k8s_role.metadata.0.name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.k8s_service_account.metadata.0.name
    namespace = kubernetes_namespace.k8s_namespace.metadata.0.name
  }

}

