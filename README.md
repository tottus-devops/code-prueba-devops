# Prueba DevOps Tottus
### Instalar Dependencias
```bash
$ npm install
```
#### Evidencia:
![pic01](./evidencias/pic01.png)
### Ejecutar Test

```bash
$ npm run test
```
#### Evidencia:
![pic02](./evidencias/pic02.png)
### Ejecutar la aplicación

```bash
$ node index.js
Example app listening on port 3000!
```
#### Evidencia:
![pic03](./evidencias/pic03.png)

![pic04](./evidencias/pic04.png)

## Etapa 1 Contenedores

  1. Crear un dockerfile que generé la imagen más pequeña y segura posible. [Dockerfile](Dockerfile)
  ```Dockerfile
  FROM node:14-alpine
  WORKDIR /app
  EXPOSE 3000
  COPY ./package.json ./
  RUN npm install --only=production
  COPY index.js index.js
  RUN addgroup -S devops && adduser -S devops -G devops
  RUN chown -R devops:devops /app
  USER devops
  CMD [ "npm", "start" ]
  ```
  2. Desplegar la aplicación y probarla en los puertos 3000 y 2375. [docker-compose.yml](docker-compose.yml)
  ```yaml
  version: '3'
  services:
    app:
      build: 
        context: .
      image: devops-app:latest
      restart: always
      ports: 
        - "3000:3000"
        - "2375:3000"
  ```
  ```bash
  $ docker-compose build
  $ docker-compose up   
  Recreating prueba-devops_app_1 ... done
  Attaching to prueba-devops_app_1
  app_1  | 
  app_1  | > basicservice@1.0.0 start /app
  app_1  | > node index.js
  app_1  | 
  app_1  | Listening on port 3000!
  ```

  #### Evidencia:
  ![pic05](./evidencias/pic05.png)
  3. Coméntanos tu experiencia de cada paso y cuales fueron los resultados obtenidos. 

  * Los pasos fueron prácticos para ejecutar y bastante rápidos de configurar, dejo evidencias de lo ejecutado.

## Etapa 2 Kubernetes

  1. Utiliza minikube o microk8s para la elaboración de esta etapa, esto nos permitirá corregirla más rápidamente.
  ```bash
  $ sudo snap install microk8s --classic
  $ microk8s enable dns dashboard storage registry rbac
  ```
  2. Crear los manifiestos necesarios para desplegar la aplicación y poder probarla. Documenta los yaml y como lograste consumir la aplicación.
  [manifest_deployment.yaml](manifest_deployment.yaml)
  ```yaml
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: http-ingress
  spec:
    defaultBackend:
      service:
        name: devops-app
        port:
          number: 3000
  ---
  apiVersion: v1
  kind: Service
  metadata:
    name: devops-app
    labels:
      app: devops-app
  spec:
    ports:
      - port: 3000
        targetPort: 3000
    selector:
      app: devops-app
    type: ClusterIP
  ---
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: devops-app
  spec:
    selector:
      matchLabels:
        app: devops-app
    replicas: 1
    strategy:
      type: RollingUpdate
      rollingUpdate:
        maxUnavailable: 0
        maxSurge: 1
    template:
      metadata:
        labels:
          app: devops-app
      spec:
        containers:
          - name: devops-app
            image: localhost:32000/devops-app:latest
            ports:
              - containerPort: 3000
            resources:
              requests:
                cpu: "250m"
  ---
  apiVersion: autoscaling/v2beta2
  kind: HorizontalPodAutoscaler
  metadata:
    name: devops-app
  spec:
    scaleTargetRef:
      apiVersion: apps/v1
      kind: Deployment
      name: devops-app
    minReplicas: 1
    maxReplicas: 4
    metrics:
      - type: Resource
        resource:
          name: cpu
          target:
            type: Utilization
            averageUtilization: 50
  ```
  ```bash
  $ docker-compose push app
  $ microk8s config > ~/.kube/config
  $ kubectl apply -f manifest_deployment.yaml
  ingress.networking.k8s.io/http-ingress configured
  service/devops-app configured
  deployment.apps/devops-app configured
  horizontalpodautoscaler.autoscaling/devops-app configured
  ```
  #### Evidencia:
  ![pic06](./evidencias/pic06.png)
  ![pic07](./evidencias/pic07.png)
  ![pic09](./evidencias/pic09.png)

  3. Luego de probar los manifiestos, utiliza helm para empaquetar tu solución y pruébala de esta forma.
  ```bash
  $ helm create node-chart
  ```
  El archivo `values.yml` se modifica con los valores necesarios para el despliegue [node-chart/values.yaml](node-chart/values.yaml)
  ```yaml
  # node-chart/values.yaml
  replicaCount: 1
  image:
    repository: localhost:32000/devops-app
    pullPolicy: IfNotPresent
    tag: latest
  imagePullSecrets: []
  nameOverride: ""
  fullnameOverride: ""
  serviceAccount:
    create: false
    annotations: {}
    name: ""
  podAnnotations: {}
  podSecurityContext: {}
  securityContext: {}
  service:
    type: ClusterIP
    port: 3000
  ingress:
    enabled: true
    annotations: {}
    hosts:
      - host: node-app.local
        paths:
          - path: /
            pathType: ImplementationSpecific
    tls: []
  resources: {}
  autoscaling:
    enabled: true
    minReplicas: 1
    maxReplicas: 4
    targetCPUUtilizationPercentage: 50
  nodeSelector: {}
  tolerations: []
  affinity: {}
  ```
  ```bash
  $ helm install node-app node-chart/ --namespace node-app-ns --create-namespace
  ```
  #### Evidencia:
  ![pic10](./evidencias/pic10.png)
  ![pic11](./evidencias/pic11.png)
  ![pic12](./evidencias/pic12.png)
    
  4. **BONUS**: Cuéntanos que es lo que más te gusta de kubernetes y si pudieras, ¿qué le cambiarias?
  
  * Kubernetes es una tecnologia muy práctica para implementar enfoques de CI/CD, en general no tengo algo en especifico para cambiar, lo que si agregaría es la capacidad de un despliegue multi región y multi cloud con un lenguaje unificado.

## Etapa 3 Terraform

  1. Un rol personalizado en kubernetes.
  2. Una cuenta de servicio asociada al rol en kubernetes y que tenga permisos sobre un namespace en particular.
  
  [terraform/main.tf](terraform/main.tf)
  ```zsh
  provider "kubernetes" {
    config_path    = "~/.kube/config"
    config_context = "microk8s"
  }

  resource "kubernetes_role" "k8s_role" {
    metadata {
      name = "terraform-k8s-role"
      namespace = "node-app-ns"
    }
    rule {
      api_groups     = [""]
      resources      = ["*"]
      verbs          = ["*"]
    }
  }

  resource "kubernetes_service_account" "k8s_service_account" {
    metadata {
      name = "terraform-k8s-service-account"
      namespace = "node-app-ns"
    }
  }

  resource "kubernetes_role_binding" "k8s_role_binding" {
    metadata {
      name      = "terraform-k8s-role-binding"
      namespace = "node-app-ns"
    }
    role_ref {
      api_group = "rbac.authorization.k8s.io"
      kind      = "Role"
      name      = kubernetes_role.k8s_role.metadata.0.name
    }
    subject {
      kind      = "ServiceAccount"
      name      = kubernetes_service_account.k8s_service_account.metadata.0.name
      namespace = "node-app-ns"
    }
  }
  ```
  ```bash
  $ terraform init
  $ terraform plan
  $ terraform apply
  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
    + create

  Terraform will perform the following actions:

    # kubernetes_role.k8s_role will be created
    + resource "kubernetes_role" "k8s_role" {
        + id = (known after apply)

        + metadata {
            + generation       = (known after apply)
            + name             = "terraform-k8s-role"
            + namespace        = "node-app-ns"
            + resource_version = (known after apply)
            + uid              = (known after apply)
          }

        + rule {
            + api_groups = [
                + "",
              ]
            + resources  = [
                + "*",
              ]
            + verbs      = [
                + "*",
              ]
          }
      }

    # kubernetes_role_binding.k8s_role_binding will be created
    + resource "kubernetes_role_binding" "k8s_role_binding" {
        + id = (known after apply)

        + metadata {
            + generation       = (known after apply)
            + name             = "terraform-k8s-role-binding"
            + namespace        = "node-app-ns"
            + resource_version = (known after apply)
            + uid              = (known after apply)
          }

        + role_ref {
            + api_group = "rbac.authorization.k8s.io"
            + kind      = "Role"
            + name      = "terraform-k8s-role"
          }

        + subject {
            + api_group = (known after apply)
            + kind      = "ServiceAccount"
            + name      = "terraform-k8s-service-account"
            + namespace = "node-app-ns"
          }
      }

    # kubernetes_service_account.k8s_service_account will be created
    + resource "kubernetes_service_account" "k8s_service_account" {
        + automount_service_account_token = true
        + default_secret_name             = (known after apply)
        + id                              = (known after apply)

        + metadata {
            + generation       = (known after apply)
            + name             = "terraform-k8s-service-account"
            + namespace        = "node-app-ns"
            + resource_version = (known after apply)
            + uid              = (known after apply)
          }
      }

  Plan: 3 to add, 0 to change, 0 to destroy.
  kubernetes_service_account.k8s_service_account: Creating...
  kubernetes_role.k8s_role: Creating...
  kubernetes_role.k8s_role: Creation complete after 0s [id=node-app-ns/terraform-k8s-role]
  kubernetes_service_account.k8s_service_account: Creation complete after 1s [id=node-app-ns/terraform-k8s-service-account]
  kubernetes_role_binding.k8s_role_binding: Creating...
  kubernetes_role_binding.k8s_role_binding: Creation complete after 0s [id=node-app-ns/terraform-k8s-role-binding]

  Apply complete! Resources: 3 added, 0 changed, 0 destroyed.
  ```

  #### Evidencia:
  ![pic13](./evidencias/pic13.png)

  3. **BONUS**: Demuéstranos como crearías el cluster k8s más pequeño posible en tu nube favorita! Justifica el tamaño que elegiste. 
  * En la nube de Azure en la mayoria de los casos utilizo el tamaño `D4s_v3` que tiene como caracteristicas `CPU:4/RAM:16GiB/Almacenamiento:32GiB`, esto ayuda en el proceso de escalado elástico al distribuir la carga de uso concurrente.

## Etapa 4 Construcción de CI/CD

  1. Crea un flujo exitoso para la aplicación utilizando la herramienta que mejor domines o consideres.
  2. Automatiza la etapa 3, logrando crear infraestructura de forma segura y automatizada.
  ```bash
  $ curl --resolve 'node-app.local:80:20.81.91.202' http://node-app.local/ -s | jq         
  {
    "msg": "Easy peasy lemon squeezy ;)"
  }
  $ curl --resolve 'node-app.local:80:20.81.91.202' http://node-app.local/cheers -s | jq   
  {
    "msg": "You can do it :)"
  }
  $ curl --resolve 'node-app.local:80:20.81.91.202' http://node-app.local/private -s | jq
  {
    "private_token": "VmFzIG11eSBiaWVuIQo="
  }
  ```
  3. **BONUS**: Utilizar Gitlab-CI te dará puntos extra, ya que es nuestra herramienta de trabajo actual, adicionalmente para esto te pedimos que instales un runner de gitlab en tu clúster creado en los pasos anteriores. ¡Cuéntanos como te fue con esto! 


¡Te deseamos el mejor de los éxitos!
